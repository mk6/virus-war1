#include "cell.h"

#define ROWS 10
#define COLS 10

Cell::Cell(int x, int y, int cost)
{
    this -> x    = x;
    this -> y    = y;
    this -> cost = cost;
}

/*
Cell::Cell(int xx, int yy)
{
    this -> x = xx;
    this -> y = yy;
}
*/

bool Cell::isValid(Cell c)
{
    return !(c.x < 0 || c.x >= ROWS || c.y < 0 || c.y >= COLS);
}

int Cell::findPos(Cell c, std::vector <Cell> &source)
{
    for (int i = 0; i < int(source.size()); i++) {
        if (source[i].x == c.x && source[i].y == c.y)
            return i;
    }
    return -1;
}
