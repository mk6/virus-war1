#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include "player.h"
#include "cell.h"

namespace Ui {
class GameWindow;
}

class GameWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = nullptr);
    ~GameWindow();

    bool isCellAvailable(int x, int y, bool isHuman);
    bool isFortress(int x, int y);
    bool checkFortressChain(std::vector <Cell> &awaits, bool isHuman);
    bool isValid(int x, int y);

    void updateCell(int x, int y, bool isHuman);
    void scanInRadius(int x, int y, int radius, int &crosses, int &circles, int &crFt, int &clFt);
    void processAi();
    void findFortress(int x, int y, bool isHuman, std::vector <Cell> &destination);

    int calculateCellCost(int x, int y);

    Cell findBestCell(bool isHuman);

public slots:
    void onCellClick(int x, int y);
    void restartButtonClicked();

private:
    Ui::GameWindow *ui;

    Player player;
    Player ai;
};

#endif // GAMEWINDOW_H
