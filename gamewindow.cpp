#include "gamewindow.h"
#include "ui_gamewindow.h"

#define ROWS ui->gameField->rowCount()
#define COLS ui->gameField->columnCount()
#define UiCell ui->gameField->item

GameWindow::GameWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{
    ui -> setupUi(this);

    this -> setFixedSize(QSize(700, 430));

    // Create table items
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            ui -> gameField -> setItem(i, j, new QTableWidgetItem);
            UiCell(i, j) -> setTextAlignment(Qt::AlignCenter);
        }
    }

    // Initialize start positions
    UiCell(0, 0) -> setText("X");
    UiCell(ROWS - 1, COLS - 1) -> setText("O");

    // Allow AI to go first
    /*
    emit processAi();
    ai.stepCounter = 0;
    player.stepCounter = 1;
    */
}

GameWindow::~GameWindow()
{
    delete ui;
}

/// Processes user's clicks
void GameWindow::onCellClick(int x, int y)
{
    Cell checkCell(0, 0, 500);

    if (player.stepCounter < 3 && isCellAvailable(x, y, true)) {
        updateCell(x, y, true);
        player.stepCounter++;
        //player.cellsAlive++;
    }

    if (ai.cellsAlive == 0) {
        ui -> statusLabel -> setText("Crosses won!");
        ui -> gameField -> setEnabled(false);
    } else if (player.cellsAlive == 0) {
        ui -> statusLabel -> setText("Circles won!");
        ui -> gameField -> setEnabled(false);
    }

    if (player.stepCounter == 3) {
        processAi();
        player.stepCounter = 0;
        ai.stepCounter = 0;
    }

    checkCell = findBestCell(true);
    if (checkCell.x == -1) {
        ui -> statusLabel -> setText("Circles won!");
        ui -> gameField -> setEnabled(false);
        return;
    }
}

/// Checks if Cell is not a fortress, is not already occupied, is within game field
/// and if it is near at least one alive cell or living fortress chain
bool GameWindow::isCellAvailable(int x, int y, bool isHuman)
{
    QString myStr;

    int crosses = 0;
    int circles = 0;
    int crFt = 0;
    int clFt = 0;

    std::vector <Cell> c;
    findFortress(x, y, isHuman, c);

    isHuman ? myStr = "X" : myStr = "O";

    if (!(isValid(x, y)) || UiCell(x, y) -> text() == myStr || isFortress(x, y))
        return false;

    scanInRadius(x, y, 1, crosses, circles, crFt, clFt);
    if (isHuman) {
        if (crosses > 0) {
            return true;
        }
        if (clFt > 0) {            
            if (checkFortressChain(c, isHuman)) {
                return true;

            }
        }
    } else {
        if (circles > 0) {
            return true;
        }
        if (crFt > 0) {
            if (checkFortressChain(c, isHuman)) {
                return true;
            }
        }
    }

    return false;
}


/// Updates cell text and color dependent on its current state and which side's turn it is
void GameWindow::updateCell(int x, int y, bool isHuman)
{
    QString friendCell;
    QString enemyCell;
    QColor fortressColor;

    if (isHuman) {
        friendCell    = "X";
        enemyCell     = "O";
        fortressColor = Qt::blue;
    } else {
        friendCell    = "O";
        enemyCell     = "X";
        fortressColor = Qt::red;
    }

    if (UiCell(x, y) -> text() == "") {
        UiCell(x, y) -> setText(friendCell);
        if (isHuman) {
            player.cellsAlive++;
        } else {
            ai.cellsAlive++;
        }
    } else {
        UiCell(x, y) -> setBackgroundColor(fortressColor);
        isHuman ? ai.cellsAlive-- : player.cellsAlive--;
    }

}


/// Checks if given cell is a fortress (doesn't count the side of the 'conflict')
bool GameWindow::isFortress(int x, int y)
{
    if (UiCell(x, y) -> backgroundColor() == Qt::blue || UiCell(x, y) -> backgroundColor() == Qt::red)
        return true;
    return false;
}

/// 'Scans' distance inside square of side = 2*radius + 1, counts nearby cells of all types except the empty ones
void GameWindow::scanInRadius(int x, int y, int radius, int &crosses, int &circles, int &crFt, int &clFt)
{
    for (int i = x - radius; i <= x + radius; i++) {
        for (int j = y - radius; j <= y + radius; j++) {

            if (!(isValid(i, j)))
                continue;

            if (UiCell(i, j) -> text() == "X") {
                if (isFortress(i, j)) {
                    crFt++;
                } else {
                    crosses++;
                }
            } else if (UiCell(i, j) -> text() == "O") {
                if (isFortress(i, j)) {
                    clFt++;
                } else {
                    circles++;
                }
            }
        }
    }
}

// Checks if fortress chain is 'alive', meaning that we can spawn viruses near or eat enemies
bool GameWindow::checkFortressChain(std::vector <Cell> &awaits, bool isHuman)
{
    std::vector <Cell> visited;
    //std::vector <Cell> awaits;

    QString friendCell;
    QString enemyCell;

    if (isHuman) {
        friendCell = "X";
        enemyCell  = "O";
    } else {
        friendCell = "O";
        enemyCell  = "X";
    }
    //awaits.push_back(Cell(xStart, yStart));

    while (awaits.size() > 0) {
        for (int i = awaits[0].x - 1; i <= awaits[0].x + 1; i++) {
            for (int j = awaits[0].y - 1; j <= awaits[0].y + 1; j++) {
                if (!isValid(i, j)) {
                    continue;
                }
                if (isFortress(i, j) && UiCell(i, j) -> text() == enemyCell && Cell::findPos(Cell(i, j), visited) == -1) {
                    awaits.push_back(Cell(i, j));
                } else if (UiCell(i, j) -> text() == friendCell && !(isFortress(i, j))) {
                    return true;
                }
            }
        }

        visited.push_back(awaits[0]);
        awaits.erase(awaits.begin());
    }

    return false;
}

/// Checks if cell is within game field
bool GameWindow::isValid(int x, int y)
{
    return !(x < 0 || x >= ROWS || y < 0 || y >= COLS);
}


/// Returns cell with positions of first found fortress or (-1; -1) if there's no any
void GameWindow::findFortress(int x, int y, bool isHuman, std::vector <Cell> &desination)
{
    QString checkItem;

    isHuman ? checkItem = "O" : checkItem = "X";

    for (int i = x - 1; i <= x + 1; i++) {
        for (int j = y - 1; j <= y + 1; j++) {
            if (!isValid(i, j))
                continue;
            if (isFortress(i, j) && UiCell(i, j) -> text() == checkItem) {
                desination.push_back(Cell(i, j));
            }
        }
    }
    return;
}

void GameWindow::processAi()
{
    while (ai.stepCounter < 3 && ai.cellsAlive > 0) {
        Cell c = findBestCell(false);
        if (c.x == -1) {
            ui -> statusLabel -> setText("Crosses won!");
            ui -> gameField -> setEnabled(false);
            return;
        }
        updateCell(c.x, c.y, false);

        ai.stepCounter++;
    }
    if (ai.cellsAlive == 0) {
        ui -> statusLabel -> setText("Crosses won!");
    } else if (player.cellsAlive == 0) {
        ui -> statusLabel -> setText("Circles won!");
    }
}

Cell GameWindow::findBestCell(bool isHuman)
{
    Cell c(-1, -1, 600);

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (isCellAvailable(i, j, isHuman)) {
                Cell t(i, j, calculateCellCost(i, j));
                if (t <= c) {
                    c = t;
                }
            }
        }
    }

    return c;
}

int GameWindow::calculateCellCost(int x, int y) {
    int crosses = 0;
    int circles = 0;
    int crFt = 0;
    int clFt = 0;

    scanInRadius(x, y, ui->radiusInput->value(), crosses, circles, crFt, clFt);

    if (UiCell(x, y)->text() == "X" && !isFortress(x, y) && ui->forceAttackCheckBox->isChecked()) {
        if (ui->crossesInput->value() < 0) {
            crosses *= 100;
        } else {
            crosses *= -100;
        }
    }

    return ui->circlesInput->value() * circles +
           ui->circleFortsInput->value() * clFt +
           ui->crossesInput->value() * crosses +
           ui->crossFortsInput->value() * crFt;
    //return -crosses - crFt;
}

void GameWindow::restartButtonClicked()
{
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            UiCell(i, j)->setBackgroundColor(Qt::white);
            UiCell(i, j)->setText("");
        }
    }

    UiCell(0, 0)->setText("X");
    UiCell(ROWS - 1, COLS - 1)->setText("O");

    player.stepCounter = 1;
    ai.stepCounter = 1;
    ui->gameField->setEnabled(true);
}
