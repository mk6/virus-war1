#ifndef CELL_H
#define CELL_H

#include <vector>

class Cell
{
public:
    int x;
    int y;
    int cost;

    static int findPos(Cell c, std::vector <Cell> &source);

    Cell(int x, int y, int cost=100);

    static bool isValid(Cell c);
    //static bool isValid(int x, int y);

    bool operator < (const Cell &cell) const
    {
        return (cost < cell.cost);
    }

    bool operator <= (const Cell &cell) const
    {
        return (cost <= cell.cost);
    }

    bool operator > (const Cell &cell) const
    {
        return (cost > cell.cost);
    }

    bool operator == (const Cell &cell) const
    {
        return (cost == cell.cost);
    }
};

#endif // CELL_H
